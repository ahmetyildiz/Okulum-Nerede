package feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede.Services;

import java.util.List;

import feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede.Models.District;
import feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede.Models.School;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by NFL on 3.05.2017.
 */

public interface RetrofitInterface {
    @GET("getDistricts")
    Call<List<District>> getDistricts();

    @GET("getSchools")
    Call<List<School>> getSchool(@Query("i_district")Integer i_district,
                                 @Query("school_type")String schoolType,
                                 @Query("school_name") String schoolName
                                 );
}
