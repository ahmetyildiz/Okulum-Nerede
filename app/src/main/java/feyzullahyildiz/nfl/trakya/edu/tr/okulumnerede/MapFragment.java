package feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.Basemap;
import com.esri.arcgisruntime.mapping.view.Callout;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.LocationDisplay;
import com.esri.arcgisruntime.mapping.view.MapView;
import com.esri.arcgisruntime.symbology.SimpleMarkerSymbol;
import com.esri.arcgisruntime.symbology.SimpleRenderer;


/**
 * Created by NFL on 7.05.2017.
 */

public class MapFragment extends Fragment {
    private static final String ARG_LON = "arg_i_lon";
    private static final String ARG_LAT = "arg_lan";
    private static final String ARG_SCHOOL_NAME = "arg_school_name";

    private static final int LOCATION_REQUEST_CODE = 1000;

    private String[] reqPermissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    private MapView mMapView;
    private LocationDisplay mLocationDisplay;

    private ImageButton mNavigateImageButton;
    private ImageButton mMyLocationImageButton;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        init(view);
        ArcGISMap arcGISMap = new ArcGISMap(Basemap.createImagery());
        mMapView.setMap(arcGISMap);

        mLocationDisplay = mMapView.getLocationDisplay();
        mLocationDisplay.setInitialZoomScale(15);

        mLocationDisplay.addDataSourceStatusChangedListener(new LocationDisplay.DataSourceStatusChangedListener() {
            @Override
            public void onStatusChanged(LocationDisplay.DataSourceStatusChangedEvent dataSourceStatusChangedEvent) {
                if (dataSourceStatusChangedEvent.isStarted())
                    return;

                if (dataSourceStatusChangedEvent.getError() == null)
                    return;

                boolean permissionCheck1 = ContextCompat.checkSelfPermission(getActivity(), reqPermissions[0]) ==
                        PackageManager.PERMISSION_GRANTED;
                boolean permissionCheck2 = ContextCompat.checkSelfPermission(getActivity(), reqPermissions[1]) ==
                        PackageManager.PERMISSION_GRANTED;
                if (!(permissionCheck1 && permissionCheck2)) {
                    ActivityCompat.requestPermissions(getActivity(), reqPermissions, LOCATION_REQUEST_CODE);
                } else {
                    Toast.makeText(getActivity(), getString(R.string.error_message), Toast.LENGTH_SHORT).show();
                }

            }
        });

        mMyLocationImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLocationDisplay.startAsync();
            }
        });

        // eğer konum bilgisi gönderilmiş ise navigasyon kurma butonunu görünür yap
        if (getArguments().getString(ARG_SCHOOL_NAME) != null) {
            mNavigateImageButton.setVisibility(View.VISIBLE);
            double latitude = getArguments().getDouble(ARG_LAT);
            double longitude = getArguments().getDouble(ARG_LON);
            String schoolName = getArguments().getString(ARG_SCHOOL_NAME);
            drawSimpleMarkerSymbol(latitude, longitude, schoolName);
        }
        mNavigateImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double latitude = getArguments().getDouble(ARG_LAT);
                double longitude = getArguments().getDouble(ARG_LON);
                String schoolName = getArguments().getString(ARG_SCHOOL_NAME);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                String data = String.format("geo:%s,%s?q=%s", latitude, longitude, schoolName);
                intent.setData(Uri.parse(data));
                startActivity(intent);
            }
        });

        mLocationDisplay.startAsync();
        return view;
    }

    private void drawSimpleMarkerSymbol(double lat, double lon, String schoolName) {

        Point schoolPoint = new Point(lon, lat, SpatialReferences.getWgs84());
        GraphicsOverlay graphicOverlay = new GraphicsOverlay();
        mMapView.getGraphicsOverlays().add(graphicOverlay);

        SimpleMarkerSymbol symbol = new SimpleMarkerSymbol(SimpleMarkerSymbol.Style.CIRCLE, Color.RED, 15);
        SimpleRenderer renderer = new SimpleRenderer(symbol);
        graphicOverlay.setRenderer(renderer);

        Graphic graphic = new Graphic(schoolPoint);
        graphicOverlay.getGraphics().add(graphic);
        TextView textView = new TextView(getActivity());
        textView.setText(schoolName);

        Callout callout = mMapView.getCallout();
        callout.setLocation(schoolPoint);
        callout.setContent(textView);
        callout.show();

    }

    private void init(View view) {
        mMapView = (MapView) view.findViewById(R.id.fragment_map_map_view);
        mNavigateImageButton = (ImageButton) view.findViewById(R.id.fragment_map_navigate_image_button);
        mMyLocationImageButton = (ImageButton) view.findViewById(R.id.fragment_map_my_location_image_button);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.top_menu, menu);

        //actionbardaki arama yaptığımız yer
        MenuItem searchItem = menu.findItem(R.id.top_menu_search_view);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //arama olarak sadece okul adı parametre olarak gönderiliyor.
                Intent i = SchoolsListActivity.newIntent(getActivity(), null, query, null);
                startActivity(i);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.top_menu_detail_search:
                Intent intent = DetailSearchActivity.newIntent(getActivity());
                startActivity(intent);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    public static MapFragment newInstance(String schoolName, double lon, double lat) {
        Bundle args = new Bundle();
        args.putString(ARG_SCHOOL_NAME, schoolName);
        args.putDouble(ARG_LAT, lat);
        args.putDouble(ARG_LON, lon);
        MapFragment fragment = new MapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.resume();
    }


}