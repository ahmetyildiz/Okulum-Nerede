package feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

/**
 * Created by NFL on 3.05.2017.
 */

public class SchoolsListActivity extends SingleFragmentActivity {

    private static final String EXTRA_I_DISTRICT = "extra_i_district";
    private static final String EXTRA_SCHOOL_TYPE = "extra_school_type";
    private static final String EXTRA_SCHOOL_NAME = "extra_school_name";

    @Override
    protected Fragment createFragment() {
        Integer i_district = getIntent().getIntExtra(EXTRA_I_DISTRICT, 0);
        String school_type = getIntent().getStringExtra(EXTRA_SCHOOL_TYPE);
        String school_name = getIntent().getStringExtra(EXTRA_SCHOOL_NAME);
        return SchoolsListFragment.newInstance(i_district, school_type, school_name);
    }

    public static Intent newIntent(Context context, Integer i_dis, String schoolName, String schoolType) {
        Intent i = new Intent(context,SchoolsListActivity.class);
        i.putExtra(EXTRA_SCHOOL_NAME,schoolName);
        i.putExtra(EXTRA_SCHOOL_TYPE,schoolType);
        i.putExtra(EXTRA_I_DISTRICT,i_dis);

        return i;
    }

}
