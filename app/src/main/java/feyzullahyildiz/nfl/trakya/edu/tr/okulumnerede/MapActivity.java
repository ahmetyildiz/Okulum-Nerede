package feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

/**
 * Created by NFL on 7.05.2017.
 */

public class MapActivity extends SingleFragmentActivity {

    private static final String EXTRA_LON = "extra_i_lon";
    private static final String EXTRA_LAT = "extra_lan";
    private static final String EXTRA_SCHOOL_NAME = "extra_school_name";

    @Override
    protected Fragment createFragment() {
        String schoolName = getIntent().getStringExtra(EXTRA_SCHOOL_NAME);
        double lon = getIntent().getDoubleExtra(EXTRA_LON, 0);
        double lat = getIntent().getDoubleExtra(EXTRA_LAT, 0);
        return MapFragment.newInstance(schoolName, lon, lat);
    }
    public static Intent newIntent(Context context, double lon, double lat, String schoolName) {
        Intent i = new Intent(context, MapActivity.class);
        i.putExtra(EXTRA_LON, lon);
        i.putExtra(EXTRA_LAT, lat);
        i.putExtra(EXTRA_SCHOOL_NAME, schoolName);

        return i;
    }
}
