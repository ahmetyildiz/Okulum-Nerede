package feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

/**
 * Created by NFL on 3.05.2017.
 */

public class DetailSearchActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return DetailSearchFragment.newInstance();
    }

    public static Intent newIntent(Context context) {
        Intent i = new Intent(context, DetailSearchActivity.class);
        return i;
    }
}
