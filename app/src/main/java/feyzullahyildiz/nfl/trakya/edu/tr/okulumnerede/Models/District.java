package feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede.Models;

/**
 * Created by NFL on 3.05.2017.
 */

public class District {


    /**
     * i_district : 1103
     * district : ADALAR
     */

    private Integer i_district;
    private String district;

    public District(Integer i_district, String district) {
        this.i_district = i_district;
        this.district = district;
    }

    public Integer getI_district() {
        return i_district;
    }

    public void setI_district(Integer i_district) {
        this.i_district = i_district;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    @Override
    public String toString() {
        return getDistrict();
    }
}
