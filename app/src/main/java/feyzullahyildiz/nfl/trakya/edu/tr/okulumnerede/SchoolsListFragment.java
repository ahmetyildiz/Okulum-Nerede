package feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede.Models.School;
import feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede.Services.RetrofitInterface;
import feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede.Services.WS;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by NFL on 3.05.2017.
 */

public class SchoolsListFragment extends Fragment {
    private static final String ARG_I_DISTRICT = "arg_i_district";
    private static final String ARG_SCHOOL_TYPE = "arg_school_type";
    private static final String ARG_SCHOOL_NAME = "arg_school_name";


    private RetrofitInterface service = WS.getService();
    private RecyclerView mSchoolsRecyclerView;
    private SchoolAdapter mSchoolAdapter;


    public static SchoolsListFragment newInstance(Integer iDistrict, String schoolType, String schoolName) {

        Bundle args = new Bundle();
        args.putInt(ARG_I_DISTRICT, iDistrict);
        args.putString(ARG_SCHOOL_NAME, schoolName);
        args.putString(ARG_SCHOOL_TYPE, schoolType);

        SchoolsListFragment fragment = new SchoolsListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_school_list, container, false);
        init(view);
        Integer i_distric = getArguments().getInt(ARG_I_DISTRICT);
        String schoolName = getArguments().getString(ARG_SCHOOL_NAME);
        String schoolType = getArguments().getString(ARG_SCHOOL_TYPE);

        service.getSchool(i_distric, schoolType, schoolName).enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                updateUI(response.body());
            }

            @Override
            public void onFailure(Call<List<School>> call, Throwable t) {

            }
        });

        return view;
    }

    private void updateUI(List<School> schoolList) {
        mSchoolAdapter = new SchoolAdapter(schoolList);
        mSchoolsRecyclerView.setAdapter(mSchoolAdapter);
    }

    private class SchoolHolder extends RecyclerView.ViewHolder {
        private School mSchool;

        private TextView mSchoolName;
        private TextView mSchoolType;
        private TextView mSchoolDistrictName;

        private ImageButton mSchoolNavigate;
        private ImageButton mSchoolShowOnMap;

        public SchoolHolder(View itemView) {
            super(itemView);
            mSchoolName = (TextView) itemView.findViewById(R.id.fragment_school_list_item_school_name_text_view);
            mSchoolType = (TextView) itemView.findViewById(R.id.fragment_school_list_item_school_type_text_view);
            mSchoolDistrictName = (TextView) itemView.findViewById(R.id.fragment_school_list_item_school_district_text_view);
            mSchoolNavigate = (ImageButton) itemView.findViewById(R.id.fragment_school_list_item_google_maps_image_button);
            mSchoolShowOnMap = (ImageButton) itemView.findViewById(R.id.fragment_school_list_item_show_on_map);

            mSchoolNavigate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    double latitude = Double.valueOf(mSchool.getLat());
                    double longitude = Double.valueOf(mSchool.getLon());
                    /*
                    // this was only for googlemaps
                    String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)",
                            latitude, longitude, mSchool.getSchool_name());
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setPackage("com.google.android.apps.maps");
                    startActivity(intent);

                    */

                    String schoolName = getArguments().getString(ARG_SCHOOL_NAME);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    String data = String.format("geo:%s,%s?q=%s", latitude, longitude, schoolName);
                    intent.setData(Uri.parse(data));
                    startActivity(intent);
                }
            });

            mSchoolShowOnMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Double lon = Double.valueOf(mSchool.getLon());
                    Double lat = Double.valueOf(mSchool.getLat());
                    Intent i = MapActivity.newIntent(getActivity(), lon, lat, mSchool.getSchool_name());
                    startActivity(i);
                }
            });

        }

        public void bindHolder(School s) {
            mSchool = s;
            mSchoolDistrictName.setText(s.getDistrict());
            mSchoolName.setText(s.getSchool_name());
            mSchoolType.setText(s.getSchool_type());

        }


    }

    private class SchoolAdapter extends RecyclerView.Adapter<SchoolHolder> {
        private List<School> mSchools = new ArrayList<>();

        public SchoolAdapter(List<School> schoolList) {
            mSchools = schoolList;
        }

        @Override
        public SchoolHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_school_list_item, parent, false);

            return new SchoolHolder(view);
        }

        @Override
        public void onBindViewHolder(SchoolHolder holder, int position) {
            School s = mSchools.get(position);
            holder.bindHolder(s);

        }

        @Override
        public int getItemCount() {
            return mSchools.size();
        }
    }


    private void init(View view) {
        mSchoolsRecyclerView = (RecyclerView) view.findViewById(R.id.fragment_school_list_recycler_view);
        mSchoolsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }


}
