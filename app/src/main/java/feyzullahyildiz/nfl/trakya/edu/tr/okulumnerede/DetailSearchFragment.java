package feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede.Models.District;
import feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede.Services.RetrofitInterface;
import feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede.Services.WS;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by NFL on 3.05.2017.
 */

public class DetailSearchFragment extends Fragment {

    private static final String ALL_SCHOOL_TYPE_STRING = "Hepsi";

    private RetrofitInterface service = WS.getService();

    private Spinner mSchoolTypeSpinner;
    private ArrayAdapter mSchoolTypeArrayAdapter;
    private String[] mSchoolTypeNamesArray;

    private Spinner mDistrictsSpinner;
    private ArrayAdapter mDistrictsArrayAdapter;
    private List<District> mDistrictList;

    private Button mSearchDetailyButton;
    private EditText mSearchDetailySchoolNameEditText;


    public static DetailSearchFragment newInstance() {
        Bundle args = new Bundle();
        DetailSearchFragment fragment = new DetailSearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detaily_search, container, false);
        init(view);


        //ilçelerin listelenmesi
        service.getDistricts().enqueue(new Callback<List<District>>() {
            @Override
            public void onResponse(Call<List<District>> call, Response<List<District>> response) {
                mDistrictList = response.body();
                mDistrictList.add(0, new District(null, "Hepsi"));
                mDistrictsArrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, mDistrictList.toArray());
                mDistrictsSpinner.setAdapter(mDistrictsArrayAdapter);
            }

            @Override
            public void onFailure(Call<List<District>> call, Throwable t) {
                Toast.makeText(getActivity(), "Bağlantı Hatası " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        //!ilçelerin Listelenmesi

        //okul türlerinin listelenmesi
        mSchoolTypeNamesArray = new String[]{
                ALL_SCHOOL_TYPE_STRING,
                "Anadolu İmam Hatip Lisesi",
                "İlkokul",
                "Anaokulu",
                "Ortaokul",
                "Anadolu Lisesi"
        };


        mSchoolTypeArrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, mSchoolTypeNamesArray);
        mSchoolTypeSpinner.setAdapter(mSchoolTypeArrayAdapter);
        //!okul türlerinin listelenmesi

        mSearchDetailyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                District d = (District) mDistrictsSpinner.getSelectedItem();
                // selected schooltype
                String selectedSchoolType = mSchoolTypeSpinner.getSelectedItem().toString();

                // eğer seçilen okul türü hepsi ise karşıya parametre olarak null gönder
                if (selectedSchoolType.equals(ALL_SCHOOL_TYPE_STRING)) {
                    selectedSchoolType = null;
                }
                String schoolName = mSearchDetailySchoolNameEditText.getText().toString();

                Intent i = SchoolsListActivity.newIntent(getActivity(), d.getI_district(), schoolName, selectedSchoolType);
                startActivity(i);
            }
        });


        return view;
    }

    private void init(View view) {
        mDistrictsSpinner = (Spinner) view.findViewById(R.id.fragment_detaily_search_districts_spinner);
        mSchoolTypeSpinner = (Spinner) view.findViewById(R.id.fragment_detaily_search_school_type_spinner);
        mSearchDetailyButton = (Button) view.findViewById(R.id.fragment_detaily_search_button);
        mSearchDetailySchoolNameEditText = (EditText) view.findViewById(R.id.fragment_detaily_search_school_name_edit_text);
    }
/*
    private class SchoolType{
        private String mNameSchool;
        private String mValueSchool;

        public SchoolType(String nameSchool, String valueSchool) {
            mNameSchool = nameSchool;
            mValueSchool = valueSchool;
        }

        public String getNameSchool() {

            return mNameSchool;
        }

        public void setNameSchool(String nameSchool) {
            mNameSchool = nameSchool;
        }

        public String getValueSchool() {
            return mValueSchool;
        }

        public void setValueSchool(String valueSchool) {
            mValueSchool = valueSchool;
        }

        @Override
        public String toString() {
            return getNameSchool();
        }
    }
*/
}

