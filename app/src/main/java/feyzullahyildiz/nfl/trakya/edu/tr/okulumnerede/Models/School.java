package feyzullahyildiz.nfl.trakya.edu.tr.okulumnerede.Models;

/**
 * Created by NFL on 3.05.2017.
 */

public class School {


    /**
     * i_school : 16916
     * school_type : İlkokul
     * school_name : 100 Yıl Mustafa Kemal İlkokulu
     * lon : 29.0123070000001
     * lat : 41.061472
     * i_district : 1183
     * district : BEŞİKTAŞ
     */

    private int i_school;
    private String school_type;
    private String school_name;
    private String lon;
    private String lat;
    private int i_district;
    private String district;

    public int getI_school() {
        return i_school;
    }

    public void setI_school(int i_school) {
        this.i_school = i_school;
    }

    public String getSchool_type() {
        return school_type;
    }

    public void setSchool_type(String school_type) {
        this.school_type = school_type;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public int getI_district() {
        return i_district;
    }

    public void setI_district(int i_district) {
        this.i_district = i_district;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
